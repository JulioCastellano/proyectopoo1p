/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import Usuario.Comprador;
import Usuario.Usuario;
import Usuario.Vendedor;
import Vehiculo.Vehiculo;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 *
 * @author
 */
public class Proyecto {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws NoSuchAlgorithmException {
        String continuar = "";
//        String destinatario = "davmasan@espol.edu.ec"; //A quien le quieres escribir.
//
//        String asunto = "Correo  proyecto.poo.p1.2020@gmail.com";
//        String cuerpo = "passs ProyectoPOO2020...";

//        Correo.enviarConGMail(destinatario, asunto, cuerpo);
        do {
            menuPrincipal();
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                menuVendedor(continuar);
            }
            if (continuar.equals("2")) {
                menuComprador(continuar);
            }
            if (continuar.equals("3")) {
                System.out.println("Hasta luego...");
            }
        } while (!continuar.equals("3"));
    }

    private static void menuPrincipal() {
        System.out.println("****Bienvenido****");
        System.out.println("Menu de Opciones: ");
        System.out.println("    1.  Vendedor");
        System.out.println("    2.  Comprador");
        System.out.println("    3.  Salir");
    }

    private static void menuVendedor(String continuar) throws NoSuchAlgorithmException {
        do {
            System.out.println("**Menu del Vendedor**");
            System.out.println("1.  Registrar un nuevo vendedor");
            System.out.println("2.  Ingresar un nuevo vehículo");
            System.out.println("3.  Aceptar oferta");
            System.out.println("4.  Regresar");
            continuar = sc.nextLine();
            Vendedor v = null;
            if (continuar.equals("1")) {
                System.out.println("Ingrese Nombres: ");
                String nombre = sc.nextLine();
                System.out.println("Ingrese Apellidos: ");
                String apellido = sc.nextLine();
                System.out.println("Ingrese correo electronico: ");
                String email = sc.nextLine();
                System.out.println("Ingrese organizacion: ");
                String organization = sc.nextLine();
                System.out.println("Ingrese usuario: ");
                String user = sc.nextLine();
                System.out.println("Ingrese clave: ");
                String pass = sc.nextLine();
                v = new Vendedor(nombre, apellido, email, organization, user, pass);
                v.registrarUser();
            } else if (continuar.equals("2")) {
                System.out.println("Ingrese su usuario: ");
                String user = sc.nextLine();
                System.out.println("Ingrese clave: ");
                String pass = sc.nextLine();
                Util aplicacion = new Util();
                
                if (aplicacion.verificarUser(user, pass)) {
                    System.out.println("Ingreso exitoso");
                    Usuario us = new Vendedor(aplicacion.buscarUser(user, pass).getNombres(), aplicacion.buscarUser(user, pass).getApellidos(), aplicacion.buscarUser(user, pass).getCorreo(), aplicacion.buscarUser(user, pass).getOrganizacion(), aplicacion.buscarUser(user, pass).getUsuario(), aplicacion.buscarUser(user, pass).getClave());
                   
                    v = (Vendedor) us;
                    menuTipoVehiculo(continuar, v);
                } else {
                    System.out.println("Usuario o pass incorrecto");
                }
            } else if (continuar.equals("3")) {
                System.out.println("Ingrese su usuario: ");
                String user = sc.nextLine();
                System.out.println("Ingrese clave: ");
                String pass = sc.nextLine();
                Util aplicacion = new Util();
                if (aplicacion.verificarUser(user, pass)) {
                    Usuario us = new Vendedor(aplicacion.buscarUser(user, pass).getNombres(), aplicacion.buscarUser(user, pass).getApellidos(), aplicacion.buscarUser(user, pass).getCorreo(), aplicacion.buscarUser(user, pass).getOrganizacion(), aplicacion.buscarUser(user, pass).getUsuario(), aplicacion.buscarUser(user, pass).getClave());
                   
                    v = (Vendedor) us;
                    System.out.println("Ingreso exitoso");
                    System.out.println("Ingrese la placa: ");
                    String placa = sc.nextLine();
                    v.verOfertas(placa, continuar);
                }
            }
            if (continuar.equals("4")) {
                System.out.println("4.  Regresar");

            }
        } while (!continuar.equals("4"));
    }
    private static void menuTipoVehiculo(String continuar, Vendedor v) {

        System.out.println("**Tipo Vehiculo que desea registrar**");
        System.out.println("    1.  Auto");
        System.out.println("    2.  Camion");
        System.out.println("    3.  Camioneta");
        System.out.println("    4.  Moto");
        continuar = sc.nextLine();
        if (continuar.equals("1")) {
            System.out.println("Ingrese Placa: ");
            String placa = sc.nextLine();
            System.out.println("Ingrese marca: ");
            String marca = sc.nextLine();
            System.out.println("Ingrese modelo: ");
            String modelo = sc.nextLine();
            System.out.println("Ingrese Tipo de motor: ");
            String tipoMotor = sc.nextLine();
            System.out.println("Ingrese año: ");
            String anio = sc.nextLine();
            System.out.println("Ingrese recorrido: ");
            String recorrido = sc.nextLine();
            System.out.println("Ingrese color: ");
            String color = sc.nextLine();
            System.out.println("Ingrese tipo de combustible: ");
            String tcombustible = sc.nextLine();
            System.out.println("Es automatico?(SI/NO): ");
            String transmicion = sc.nextLine();
            boolean t = false;
            if (transmicion.equalsIgnoreCase("si")) {
                t = true;
            }
            System.out.println("Ingrese Precio");
            String precio = sc.nextLine();
            Util aplication = new Util();
            Vehiculo a = new Vehiculo(placa, marca, modelo, tipoMotor, anio, Integer.parseInt(recorrido), color, tcombustible, true, t, Double.parseDouble(precio),"Auto");
            v.registrarVehiculo(a);
        }
        if (continuar.equals("2")) {
            System.out.println("Ingrese Placa: ");
            String placa = sc.nextLine();
            System.out.println("Ingrese marca: ");
            String marca = sc.nextLine();
            System.out.println("Ingrese modelo: ");
            String modelo = sc.nextLine();
            System.out.println("Ingrese Tipo de motor: ");
            String tipoMotor = sc.nextLine();
            System.out.println("Ingrese año: ");
            String anio = sc.nextLine();
            System.out.println("Ingrese recorrido: ");
            String recorrido = sc.nextLine();
            System.out.println("Ingrese color: ");
            String color = sc.nextLine();
            System.out.println("Ingrese tipo de combustible: ");
            String tcombustible = sc.nextLine();
            System.out.println("Es automatico?(SI/NO): ");
            String transmicion = sc.nextLine();
            boolean t = false;
            if (transmicion.equalsIgnoreCase("si")) {
                t = true;
            }
            System.out.println("Ingrese Precio");
            String precio = sc.nextLine();
            Vehiculo a = new Vehiculo(placa, marca, modelo, tipoMotor, anio, Integer.parseInt(recorrido), color, tcombustible, true, t, Integer.parseInt(precio),"Camion");
            v.registrarVehiculo(a);
        }
        if (continuar.equals("3")) {
            System.out.println("Ingrese Placa: ");
            String placa = sc.nextLine();
            System.out.println("Ingrese marca: ");
            String marca = sc.nextLine();
            System.out.println("Ingrese modelo: ");
            String modelo = sc.nextLine();
            System.out.println("Ingrese Tipo de motor: ");
            String tipoMotor = sc.nextLine();
            System.out.println("Ingrese año: ");
            String anio = sc.nextLine();
            System.out.println("Ingrese recorrido: ");
            String recorrido = sc.nextLine();
            System.out.println("Ingrese color: ");
            String color = sc.nextLine();
            System.out.println("Ingrese tipo de combustible: ");
            String tcombustible = sc.nextLine();
            System.out.println("Es automatico?(SI/NO): ");
            String transmicion = sc.nextLine();
            boolean t = false;
            if (transmicion.equalsIgnoreCase("si")) {
                t = true;
            }
            System.out.println("Ingrese Precio");
            String precio = sc.nextLine();
            Vehiculo a = new Vehiculo(placa, marca, modelo, tipoMotor, anio, Integer.parseInt(recorrido), color, tcombustible, true, t, Integer.parseInt(precio),"Camioneta");
            v.registrarVehiculo(a);
        }
        if (continuar.equals("4")) {
            System.out.println("Ingrese Placa: ");
            String placa = sc.nextLine();
            System.out.println("Ingrese marca: ");
            String marca = sc.nextLine();
            System.out.println("Ingrese modelo: ");
            String modelo = sc.nextLine();
            System.out.println("Ingrese Tipo de motor: ");
            String tipoMotor = sc.nextLine();
            System.out.println("Ingrese año: ");
            String anio = sc.nextLine();
            System.out.println("Ingrese recorrido: ");
            String recorrido = sc.nextLine();
            System.out.println("Ingrese color: ");
            String color = sc.nextLine();
            System.out.println("Ingrese tipo de combustible: ");
            String tcombustible = sc.nextLine();

            System.out.println("Ingrese Precio");
            String precio = sc.nextLine();
            Vehiculo a = new Vehiculo(placa, marca, modelo, tipoMotor, anio, Integer.parseInt(recorrido), color, tcombustible, false, false, Double.valueOf(precio),"Moto");
            v.registrarVehiculo(a);
        } else {
            System.out.println("Ingrese un numero del 1 al 4 correspondiente a su requerimiento, porfavor");
            menuTipoVehiculo(continuar, v);
        }

    }

    private static void menuComprador(String continuar) throws NoSuchAlgorithmException {
        do {
            System.out.println("**Menu del Comprador**");
            System.out.println("1.  Registrar un nuevo Comprador");
            System.out.println("2.  Ofertar por un vehiculo");
            System.out.println("3.  Regresar");
            continuar = sc.nextLine();
            Comprador c = null;
            if (continuar.equals("1")) {
                System.out.println("Ingrese Nombres: ");
                String nombre = sc.nextLine();
                System.out.println("Ingrese Apellidos: ");
                String apellido = sc.nextLine();
                System.out.println("Ingrese correo electronico: ");
                String email = sc.nextLine();
                System.out.println("Ingrese organizacion: ");
                String organization = sc.nextLine();
                System.out.println("Ingrese usuario: ");
                String user = sc.nextLine();
                System.out.println("Ingrese clave: ");
                String pass = sc.nextLine();
                c = new Comprador(nombre, apellido, email, organization, user, pass);
                c.registrarUser();

            } else if (continuar.equals("2")) {
                System.out.println("2.Ingresar un nuevo vehículo");
                System.out.println("**Tipo Vehiculo que desea buscar**");
                System.out.println("    1.  Auto");
                System.out.println("    2.  Camion");
                System.out.println("    3.  Camioneta");
                System.out.println("    4.  Moto");
                continuar = sc.nextLine();
                String tipo = "";
                if (continuar.equals("1")) {
                    tipo = "Auto";
                }

                if (continuar.equals("2")) {
                    tipo = "Camion";
                }
                if (continuar.equals("3")) {
                    tipo = "Camioneta";
                }
                if (continuar.equals("4")) {
                    tipo = "Moto";
                }
                System.out.println("Ingrese su usuario: ");
                String user = sc.nextLine();
                System.out.println("Ingrese clave: ");
                String pass = sc.nextLine();
                Util aplicacion = new Util();
                if (aplicacion.verificarUser(user, pass)) {
                    Usuario us = new Comprador(aplicacion.buscarUser(user, pass).getNombres(), aplicacion.buscarUser(user, pass).getApellidos(), aplicacion.buscarUser(user, pass).getCorreo(), aplicacion.buscarUser(user, pass).getOrganizacion(), aplicacion.buscarUser(user, pass).getUsuario(), aplicacion.buscarUser(user, pass).getClave());
                    c = (Comprador) us;
                    System.out.println("Ingreso exitoso");

                    System.out.println("Ingrese un rango de recorrido (ej: 0-100): ");
                    String recorrido = sc.nextLine();
                    System.out.println("Ingrese un rango de año (ej: 2013-2020): ");
                    String anio = sc.nextLine();
                    System.out.println("Ingrese un rango del precio (ej: 1000-1200): ");
                    String precio = sc.nextLine();

                    c.verVehiculos(tipo, recorrido, anio, precio, continuar);
                }

            }

            if (continuar.equals("3")) {
                System.out.println("3.Regrresar");
            }

        } while (!continuar.equals("3"));
    }

}
