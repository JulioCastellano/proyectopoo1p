/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import Usuario.Comprador;
import Usuario.Usuario;
import Vehiculo.Vehiculo;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Julio Cesar
 */
public class Util {

    ArrayList<Usuario> usuarios = new ArrayList<>();
    ArrayList<Vehiculo> vehiculos = new ArrayList<>();
    ArrayList<Oferta> ofertas = new ArrayList<>();



    public ArrayList<Usuario> leerArchivoU(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File("src/Recursos/" + nombreArchivo + ".txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {

                String[] parts = linea.split(";");
                Usuario u = new Usuario(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
                if (!usuarios.contains(u)) {
                    usuarios.add(u);
                }
            }
        } catch (IOException e) {
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e2) {
            }
        }
        return usuarios;
    }

    public void escribeArchivoU(String nombreArchivo, Usuario u) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("src/Recursos/" + nombreArchivo + ".txt", true);
            pw = new PrintWriter(fichero);

            pw.print("\n"+u.getNombres() + ";" + u.getApellidos() + ";" + u.getCorreo() + ";" + u.getOrganizacion() + ";" + u.getUsuario() + ";" + toHexString(getSHA(u.getClave())));

        } catch (IOException | NoSuchAlgorithmException e) {
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (IOException e2) {
            }
        }
    }

    public  void enviarConGMail(String destinatario, String asunto, String cuerpo) {
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente también.
        String remitente = "proyecto.poo.p1.2020";  //Para la dirección nomcuenta@gmail.com

        Properties props = System.getProperties();

        props.put(
                "mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put(
                "mail.smtp.user", remitente);
        props.put(
                "mail.smtp.clave", "miClaveDeGMail");    //La clave de la cuenta
        props.put(
                "mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put(
                "mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put(
                "mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, "ProyectoPOO2020");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }

    public static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        // digest() method called  
        // to calculate message digest of an input  
        // and return array of byte 
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public String toHexString(byte[] hash) {
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);

        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros 
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }
    
    public boolean verificarUser(String usuario, String pass) throws NoSuchAlgorithmException {
        for (Usuario u : cargarUsers()) {

            if (u.getUsuario().equalsIgnoreCase(usuario) && u.getClave().equals(toHexString(getSHA(pass)))) {
               
                return true;
            }
        }
        return false;
    }public Usuario buscarUser(String usuario, String pass) throws NoSuchAlgorithmException {
        Usuario us= null;
        for (Usuario u : cargarUsers()) {

            if (u.getUsuario().equalsIgnoreCase(usuario) && u.getClave().equals(toHexString(getSHA(pass)))) {
               us=u;
                return us;
            }
        }
        return us;
    }

    public boolean verificarVehiculos(String placa) {
        for (Vehiculo v : cargarVehiculos()) {
            if (v.getPlaca().equalsIgnoreCase(placa)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Usuario> cargarUsers() {

        usuarios = this.leerArchivoU("Usuarios");

        return usuarios;
    }

    public ArrayList<Vehiculo> cargarVehiculos() {

        vehiculos = this.leerArchivoV("Vehiculos");

        return vehiculos;
    }

    public ArrayList<Oferta> cargarOfertas() {

        ofertas = this.leerArchivoO("Ofertas");

        return ofertas;
    }

    public ArrayList<Vehiculo> leerArchivoV(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            archivo = new File("src/Recursos/" + nombreArchivo + ".txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {

                String[] parts = linea.split(";");

                Vehiculo v = new Vehiculo(parts[0], parts[1], parts[2], parts[3], parts[4], Integer.parseInt(parts[5]), parts[6], parts[7], Boolean.getBoolean(parts[8]), Boolean.getBoolean(parts[9]), Double.valueOf(parts[10]),parts[11]);
                if (!vehiculos.contains(v)) {
                    vehiculos.add(v);
                }
            }
        } catch (IOException e) {
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e2) {
            }
        }
        return vehiculos;
    }

    public void agregarOferta(Oferta o) throws NoSuchAlgorithmException{
       
        this.escribeArchivoO("Ofertas", o);
    }
    public void escribeArchivoV(String nombreArchivo, Vehiculo u) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("src/Recursos/" + nombreArchivo + ".txt", true);
            pw = new PrintWriter(fichero);

            pw.print("\n"+u.getPlaca() + ";" + u.getMarca() + ";" + u.getModelo() + ";" + u.getTipoMotor() + ";" + u.getAnio() + ";" + u.getRecorrido() + ";" + u.getColor() + ";" + u.getTipoCombbustible() + ";" + u.isVidrios() + ";" + u.isTransmicion() + ";" + u.getPrecio()+";" + u.getTipo());

        } catch (IOException e) {
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (IOException e2) {
            }
        }
    }

    public ArrayList<Oferta> leerArchivoO(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            archivo = new File("src/Recursos/" + nombreArchivo + ".txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {

                String[] parts = linea.split(";");
                Comprador u = new Comprador(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
                Vehiculo v = new Vehiculo(parts[7], parts[8], parts[9], parts[10], parts[11], Integer.parseInt(parts[12]), parts[13], parts[14], Boolean.getBoolean(parts[15]), Boolean.getBoolean(parts[16]), Double.valueOf(parts[17]), parts[18]);
                Oferta o = new Oferta(u, Double.valueOf(parts[6]), v);
                if (!ofertas.contains(u)) {
                    ofertas.add(o);
                }
            }
        } catch (IOException e) {
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e2) {
            }
        }
        return ofertas;
    }

    public void escribeArchivoO(String nombreArchivo, Oferta o) throws NoSuchAlgorithmException {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("src/Recursos/" + nombreArchivo + ".txt", true);
            pw = new PrintWriter(fichero);
            Usuario u = o.getComprador();
            String usu = "\n"+u.getNombres() + ";" + u.getApellidos() + ";" + u.getCorreo() + ";" + u.getOrganizacion() + ";" + u.getUsuario() + ";" + toHexString(getSHA(u.getClave()));
            Vehiculo v = o.getVehiculo();
            String vehi = v.getPlaca() + ";" + v.getMarca() + ";" + v.getModelo() + ";" + v.getTipoMotor() + ";" + v.getAnio() + ";" + v.getRecorrido() + ";" + v.getColor() + ";" + v.getTipoCombbustible() + ";" + v.isVidrios() + ";" + v.isTransmicion() + ";" + v.getPrecio()+";"+v.getTipo();
            pw.print("\n"+usu + ";" + o.getPrecioOfertado() + ";" + vehi);

        } catch (IOException e) {
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (IOException e2) {
            }
        }
    }
}
