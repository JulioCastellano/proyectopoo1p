/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import Usuario.Comprador;
import Vehiculo.Vehiculo;

/**
 *
 * @author Luis Paredes
 */
public class Oferta {
    Comprador comprador;
    double precioOfertado;

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    Vehiculo vehiculo;

    public Oferta(Comprador comprador, double precioOfertado, Vehiculo vehiculo) {
        this.comprador = comprador;
        this.precioOfertado = precioOfertado;
        this.vehiculo = vehiculo;
    }

    

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public double getPrecioOfertado() {
        return precioOfertado;
    }

    public void setPrecioOfertado(double precioOfertado) {
        this.precioOfertado = precioOfertado;
    }

    @Override
    public String toString() {
        return "Oferta{" + "comprador=" + comprador + ", precioOfertado=" + precioOfertado + ", vehiculo=" + vehiculo + '}';
    }
    
}
