/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Proyecto.Oferta;
import Proyecto.Util;
import Vehiculo.Vehiculo;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author
 */
public class Comprador extends Usuario {

    Scanner sc = new Scanner(System.in);
    Util aplication = new Util();

    public Comprador(String nombres, String apellidos, String correo, String organizacion, String usuario, String clave) {
        super(nombres, apellidos, correo, organizacion, usuario, clave);
    }

    @Override
    public boolean userExiste(Usuario u) throws NoSuchAlgorithmException {
        return super.userExiste(u);
    }

    @Override
    public void registrarUser() {
        super.registrarUser();
    }

    public void verVehiculos(String tipo, String recorrido, String anio, String precio, String continuar) throws NoSuchAlgorithmException {
        ArrayList<Vehiculo> ve = new ArrayList<>();

        for (Vehiculo v : aplication.cargarVehiculos()) {
           
            if (!ve.contains(v)&&v.getTipo().equalsIgnoreCase(tipo)) {
               
                if (!recorrido.equals("") && !anio.equals("") && !precio.equals("")) {
                    String[] rParts = recorrido.split("-");
                    String[] pParts = precio.split("-");
                    String[] aParts = anio.split("-");
                    boolean a = Integer.parseInt(rParts[0]) <= v.getRecorrido() && Integer.parseInt(rParts[1]) >= v.getRecorrido();
                    boolean b = Integer.parseInt(aParts[0]) <= Integer.parseInt(v.getAnio()) && Integer.parseInt(rParts[1]) >= Integer.parseInt(v.getAnio());
                    boolean c = Double.parseDouble(pParts[0]) <= v.getPrecio() && Double.parseDouble(rParts[1]) >= v.getPrecio();

                    if (a && b && c) {
                        ve.add(v);
                    }
                } else if (!anio.equals("") && !precio.equals("") && recorrido.equals("")) {
                    String[] pParts = precio.split("-");
                    String[] aParts = anio.split("-");
                    boolean b = Integer.parseInt(aParts[0]) <= Integer.parseInt(v.getAnio()) && Integer.parseInt(aParts[1]) >= Integer.parseInt(v.getAnio());
                    boolean c = Double.parseDouble(pParts[0]) <= v.getPrecio() && Double.parseDouble(pParts[1]) >= v.getPrecio();

                    if (b && c) {
                        ve.add(v);
                    }
                } else if (!recorrido.equals("") && anio.equals("") && !precio.equals("")) {
                    String[] rParts = recorrido.split("-");
                    String[] pParts = precio.split("-");

                    boolean a = Integer.parseInt(rParts[0]) <= v.getRecorrido() && Integer.parseInt(rParts[1]) >= v.getRecorrido();
                    boolean c = Double.parseDouble(pParts[0]) <= v.getPrecio() && Double.parseDouble(rParts[1]) >= v.getPrecio();

                    if (a && c) {
                        ve.add(v);
                    }
                } else if (!recorrido.equals("") && !anio.equals("") && precio.equals("")) {
                    String[] rParts = recorrido.split("-");
                    String[] aParts = anio.split("-");
                    boolean a = Integer.parseInt(rParts[0]) <= v.getRecorrido() && Integer.parseInt(rParts[1]) >= v.getRecorrido();
                    boolean b = Integer.parseInt(aParts[0]) <= Integer.parseInt(v.getAnio()) && Integer.parseInt(rParts[1]) >= Integer.parseInt(v.getAnio());

                    if (a && b) {
                        ve.add(v);
                    }
                } else if (!recorrido.equals("") && anio.equals("") && precio.equals("")) {
                    String[] rParts = recorrido.split("-");

                    boolean a = Integer.parseInt(rParts[0]) <= v.getRecorrido() && Integer.parseInt(rParts[1]) >= v.getRecorrido();

                    if (a) {
                        ve.add(v);
                    }
                } else if (recorrido.equals("") && !anio.equals("") && precio.equals("")) {

                    String[] aParts = anio.split("-");
                    boolean b = Integer.parseInt(aParts[0]) <= Integer.parseInt(v.getAnio()) && Integer.parseInt(aParts[1]) >= Integer.parseInt(v.getAnio());

                    if (b) {
                        ve.add(v);
                    }
                } else if (recorrido.equals("") && anio.equals("") && !precio.equals("")) {
                    String[] pParts = precio.split("-");
                    boolean c = Double.parseDouble(pParts[0]) <= v.getPrecio() && Double.parseDouble(pParts[1]) >= v.getPrecio();

                    if (c) {
                        ve.add(v);
                    }
                } else if (recorrido.equals("") && anio.equals("") && precio.equals("")){
                    ve.add(v);
                }

            }
        }
        if (!ve.isEmpty()) {

            int i = 0;
            System.out.println("Existen " + ve.size() + " coincidencias");
            verVehiculos(continuar, ve, i, tipo);
        } else {
            System.out.println("No hay Vehiculos disponibles");
        }
    }

    private void verVehiculos(String continuar, ArrayList<Vehiculo> ve, int i, String tipo) throws NoSuchAlgorithmException {
        Vehiculo v = ve.get(i);
        System.out.println("Vehiculo " + (i + 1));
        System.out.println("Tipo: " + tipo);
        System.out.println("Precio: " + v.getPrecio());
        System.out.println("Modelo: " + v.getModelo());
        System.out.println("Año: " + v.getAnio());
                
        if ((i + 1) == ve.size()&&ve.size()>1) {

            System.out.println("1.- Anterior Vehiculo");
            System.out.println("2.- Ofertar Vehiculo");
            System.out.println("3.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i--;
                verVehiculos(continuar, ve, i, tipo);

            } else if (continuar.equals("2")) {
                System.out.println("Ingrese el precio: ");
                String ofertar = sc.nextLine();
                Oferta o = new Oferta(this, Double.valueOf(ofertar), ve.get(i));
                aplication.agregarOferta(o);
            } else {
                System.out.println("Gracias");
            }
        } else if ((i + 1) == 1 && ve.size() > 1) {
            System.out.println("1.- Siguiente Vehiculo");
            System.out.println("2.- Ofertar Vehiculo");
            System.out.println("3.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i++;
                verVehiculos(continuar, ve, i, tipo);

            } else if (continuar.equals("2")) {
                System.out.println("Ingrese el precio: ");
                String ofertar = sc.nextLine();
                Oferta o = new Oferta(this, Double.parseDouble(ofertar), ve.get(i));
                aplication.agregarOferta(o);
            } else {
                System.out.println("Gracias");
            }
        } else if ((i + 1) > 1 && i < ve.size()) {
            System.out.println("1.- Siguiente Vehiculo");
            System.out.println("2.- Anterior Vehiculo");
            System.out.println("3.- Ofertar Vehiculo");
            System.out.println("4.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i++;
                verVehiculos(continuar, ve, i, tipo);

            } else if (continuar.equals("2")) {
                i--;
                verVehiculos(continuar, ve, i, tipo);
            } else if (continuar.equals("3")) {
                System.out.println("Ingrese el precio: ");
                String ofertar = sc.nextLine();
                Oferta o = new Oferta(this, Double.parseDouble(ofertar), ve.get(i));
                aplication.agregarOferta(o);
            } else {

                System.out.println("Gracias");
            }
        } else if (ve.size() == 1) {
            System.out.println("1.- Ofertar Vehiculo");
            System.out.println("2.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                System.out.println("Ingrese el precio: ");
                String ofertar = sc.nextLine();
                Oferta o = new Oferta(this, Double.parseDouble(ofertar), ve.get(i));
                aplication.agregarOferta(o);
            } else {
                System.out.println("Gracias");
            }

        }
    }
}
