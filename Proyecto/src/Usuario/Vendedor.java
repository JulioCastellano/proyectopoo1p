/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Proyecto.Oferta;
import Proyecto.Util;
import Vehiculo.Vehiculo;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author
 */
public class Vendedor extends Usuario {

    Scanner sc = new Scanner(System.in);
    Util aplication = new Util();

    public Vendedor(String nombres, String apellidos, String correo, String organizacion, String usuario, String clave) {
        super(nombres, apellidos, correo, organizacion, usuario, clave);

    }

    @Override
    public boolean userExiste(Usuario u) throws NoSuchAlgorithmException {

        return super.userExiste(u);
    }

    @Override
    public void registrarUser() {
        super.registrarUser();
    }

    public void verOfertas(String placa, String continuar) {

        ArrayList<Oferta> o = new ArrayList<>();
        for (Oferta of : aplication.cargarOfertas()) {
            if (of.getVehiculo().getPlaca().equalsIgnoreCase(placa)) {
                o.add(of);

            }
        }
        if (!o.isEmpty()) {
            System.out.println(o.get(0).getVehiculo().getMarca() + " Precio:" + o.get(0).getVehiculo().getPrecio());
            System.out.println("Se han realizado " + o.size() + " ofertas");
            int i = 0;
            verOferta(continuar, o, i);
        } else {
            System.out.println("No tiene ofertas disponibles");
        }
    }

    private void verOferta(String continuar, ArrayList<Oferta> o, int i) {

        System.out.println("Oferta " + (i + 1));

        System.out.println("Correo:" + o.get(i).getComprador().getCorreo());
        System.out.println("Precio Ofertado: " + o.get(i).getPrecioOfertado());
        if (o.size() == 1) {
            System.out.println("1.- Aceptar Oferta");
            System.out.println("2.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                System.out.println("Se esta enviando un correo de confirmacion");

                aplication.enviarConGMail(o.get(i).getComprador().getCorreo(), "Venta de Vehiculo " + o.get(i).getVehiculo().getMarca(), "Su Oferta por $" + o.get(i).getPrecioOfertado() + " al Vehiculo " + o.get(i).getVehiculo().getMarca() + " ha sido aceptada.");
                System.out.println("Correo enviado con exito! ");
            } else {
                System.out.println("Gracias");
            }

        } else if ((i + 1) == 1 || o.size() < 1) {
            System.out.println("1.- Siguiente Oferta");
            System.out.println("2.- Aceptar Oferta");
            System.out.println("3.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i++;
                verOferta(continuar, o, i);

            } else if (continuar.equals("2")) {
                System.out.println("Se esta enviando un correo de confirmacion");

                aplication.enviarConGMail(o.get(i).getComprador().getCorreo(), "Venta de Vehiculo " + o.get(i).getVehiculo().getMarca(), "Su Oferta por $" + o.get(i).getPrecioOfertado() + " al Vehiculo " + o.get(i).getVehiculo().getMarca() + " ha sido aceptada.");
                System.out.println("Correo enviado con exito! ");
            } else {
                System.out.println("Gracias");
            }
        } else if ((i + 1) > 1 && i < o.size()) {
            System.out.println("1.- Siguiente Oferta");
            System.out.println("2.- Anterior Oferta");
            System.out.println("3.- Aceptar Oferta");
            System.out.println("4.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i++;
                verOferta(continuar, o, i);

            } else if (continuar.equals("2")) {
                i--;
                verOferta(continuar, o, i);
            } else if (continuar.equals("3")) {
                System.out.println("Se esta enviando un correo de confirmacion");

                aplication.enviarConGMail(o.get(i).getComprador().getCorreo(), "Venta de Vehiculo " + o.get(i).getVehiculo().getMarca(), "Señor " + o.get(i).getComprador().getNombres() + " " + o.get(i).getComprador().getApellidos() + " su Oferta por $" + o.get(i).getPrecioOfertado() + " al Vehiculo " + o.get(i).getVehiculo().getMarca() + " ha sido aceptada.");
                System.out.println("Correo enviado con exito! ");
            } else {

                System.out.println("Gracias");
            }
        } else if ((i + 1) == o.size() && o.size()== 2) {

            System.out.println("1.- Anterior Oferta");
            System.out.println("2.- Aceptar Oferta");
            System.out.println("3.- Regresar");
            continuar = sc.nextLine();
            if (continuar.equals("1")) {
                i--;
                verOferta(continuar, o, i);

            } else if (continuar.equals("2")) {
                System.out.println("Se esta enviando un correo de confirmacion");

                aplication.enviarConGMail(o.get(i).getComprador().getCorreo(), "Venta de Vehiculo " + o.get(i).getVehiculo().getMarca(), "Su Oferta por $" + o.get(i).getPrecioOfertado() + " al Vehiculo " + o.get(i).getVehiculo().getMarca() + " ha sido aceptada.");
                System.out.println("Correo enviado con exito! ");
            } else {
                System.out.println("Gracias");
            }
        }
    }

    public void registrarVehiculo(Vehiculo v) {
        if (!vehiculoExiste(v)) {
            aplication.escribeArchivoV("Vehiculos", v);
            System.out.println("Registro exitoso");
        } else {
            System.out.println("No se pudo realizar el registro: la placa ya existe ");
        }
    }

    public boolean vehiculoExiste(Vehiculo aThis) {
        for (Vehiculo v : aplication.cargarVehiculos()) {

            if (v.getPlaca().equalsIgnoreCase(aThis.getPlaca())) {
                return true;
            }

        }

        return false;
    }
}
