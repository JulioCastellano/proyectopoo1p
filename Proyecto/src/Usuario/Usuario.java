/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Proyecto.Util;
import Vehiculo.Vehiculo;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class Usuario {

    protected String nombres;
    protected String apellidos;
    protected String correo;
    protected String organizacion;
    protected String usuario;
    protected String clave;
    Scanner sc = new Scanner(System.in);
    Util aplication = new Util();
    Vehiculo vehiculo;
    public Usuario(String nombres, String apellidos, String correo, String organizacion, String usuario, String clave) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = clave;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public boolean userExiste(Usuario u) throws NoSuchAlgorithmException {

        for (Usuario us : aplication.cargarUsers()) {

            if (us.getCorreo().equalsIgnoreCase(u.getCorreo())) {
                return true;
            }
            if (us.getUsuario().equalsIgnoreCase(u.getUsuario())) {
                return true;
            }
        }

        return false;
    }

    public void registrarUser() {
        try {
            if (!userExiste(this)) {
                aplication.escribeArchivoU("Usuarios", this);
                System.out.println("Registro exitoso");
            } else {
                System.out.println("No se pudo realizar el registro: el usuario o el correo ya existe ");
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Vendedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.correo);
        hash = 29 * hash + Objects.hashCode(this.usuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!other.getCorreo().equalsIgnoreCase(this.correo)) {
            return false;
        }
        if (!other.getUsuario().equalsIgnoreCase(this.usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombres=" + nombres + ", apellidos=" + apellidos + ", correo=" + correo + ", organizacion=" + organizacion + ", usuario=" + usuario + ", clave=" + clave + ", sc=" + sc + ", aplication=" + aplication + ", vehiculo=" + vehiculo + '}';
    }

}
