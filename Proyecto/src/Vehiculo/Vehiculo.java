/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehiculo;

import Proyecto.Util;

/**
 *
 * @author 
 */
public class Vehiculo {
    protected String placa;
    protected String marca;
    protected String modelo;
    protected String tipoMotor;
    protected String anio;
    protected int recorrido;
    protected String color;
    protected String tipoCombbustible;
    protected boolean vidrios;
    protected boolean transmicion;
    protected double precio;
    protected String tipo;
    Util aplication=new Util();

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Vehiculo(String placa, String marca, String modelo, String tipoMotor, String anio, int recorrido, String color, String tipoCombbustible, boolean vidrios, boolean transmicion, double precio, String tipo) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.tipoMotor = tipoMotor;
        this.anio = anio;
        this.recorrido = recorrido;
        this.color = color;
        this.tipoCombbustible = tipoCombbustible;
        this.vidrios = vidrios;
        this.transmicion = transmicion;
        this.precio = precio;
        this.tipo = tipo;
    }

    
    public boolean isVidrios() {
        return vidrios;
    }

    public void setVidrios(boolean vidrios) {
        this.vidrios = vidrios;
    }

    public boolean isTransmicion() {
        return transmicion;
    }

    public void setTransmicion(boolean transmicion) {
        this.transmicion = transmicion;
    }

    
    

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipoCombbustible() {
        return tipoCombbustible;
    }

    public void setTipoCombbustible(String tipoCombbustible) {
        this.tipoCombbustible = tipoCombbustible;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + ", marca=" + marca + ", modelo=" + modelo + ", tipoMotor=" + tipoMotor + ", anio=" + anio + ", recorrido=" + recorrido + ", color=" + color + ", tipoCombbustible=" + tipoCombbustible + ", vidrios=" + vidrios + ", transmicion=" + transmicion + ", precio=" + precio + ", aplication=" + aplication + '}';
    }

  
    

   
    
  
}
